package models;

import play.*;
import play.mvc.*;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Id;


@Entity
public class Movie extends Model {

    @Id
    public Long id;

    public String nome;

    @ManyToOne
    public Genero genero;

    public static final Model.Finder<Long, Movie> find = new Model.Finder<Long, Movie>(Long.class, Movie.class);

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public Genero getGenero() {
        return genero;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setGenero(Genero genero) {
        this.genero = genero;
    }

}