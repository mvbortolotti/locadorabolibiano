package models;

import play.*;
import play.mvc.*;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Id;

import java.util.*;

@Entity
public class Genero extends Model {

    @Id
    public Long id;

    public String nome;

    @OneToMany
    public List<Movie> movies;

    public static final Model.Finder<Long, Genero>
        find = new Model.Finder<Long, Genero>(Long.class, Genero.class);

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void getMovies(List<Movie> movies) {
        this.movies = movies;
    }


}