package controllers;

import play.*;
import play.mvc.*;
import play.data.*;
import play.mvc.Results.Status;

import models.Genero;

import views.html.*;
import views.html.generos.*;

import java.util.*;

public class Generos extends Controller {

    private static Form<Genero> formCadastro = Form.form(Genero.class);

    public static Result index() {
        return ok(index.render(Genero.find.all())) ;
    }

    public static Result cadastrar() {
        Form<Genero> cadastro = formCadastro.bindFromRequest();


        if (cadastro.hasErrors()) {
            return redirect(routes.Generos.index());
        }

        cadastro.get().save();

        return redirect(routes.Generos.index());
    }

    public static Result cadastrarForm() {
        return ok(cadastrar.render(formCadastro));
    }
}