# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table genero (
  id                        bigint not null,
  nome                      varchar(255),
  constraint pk_genero primary key (id))
;

create table movie (
  id                        bigint not null,
  nome                      varchar(255),
  genero_id                 bigint,
  constraint pk_movie primary key (id))
;

create sequence genero_seq;

create sequence movie_seq;

alter table movie add constraint fk_movie_genero_1 foreign key (genero_id) references genero (id) on delete restrict on update restrict;
create index ix_movie_genero_1 on movie (genero_id);



# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists genero;

drop table if exists movie;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists genero_seq;

drop sequence if exists movie_seq;

